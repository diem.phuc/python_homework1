#EX1
print("EX1")
u, v, x, y, z = 29, 12, 10, 4, 3
print("u/v = ", u/v)
print("t = (u == v) = ", u == v)
print("u % x = ", u % x)
print("t = (x >= y) = ",  (x >= y))
u += 5
print("u += 5 = ", u)
u %=z
print("u %=z = ", u)
print("t = (v > x and y < z) = ", (v > x and y < z))
print("x**z = ", x**z)
print("x // z = ", x // z)

#EX2
print("EX2")
r = 5
pi = 3.1416
print("Perimeter = ", 2*pi*r)
print("Area = ", pi*r*r)


#Ex3

s = 'Hi John, welcome to python programming for beginner!' #a string
print("python in string =", ("python" in s))

print(s.find("John"))

s1 = s[3:7]
print("s1",s1)

print("number of o in string: ", s.count("o"))
li = s.split()
len(li)

print("number of words in the string:", len(s.split()))

#EX4
print("EX4")
s1 = 'Twinkle, twinkle, little star, How I wonder what you are! Up above the world so high, Like a diamond in the sky. Twinkle, twinkle, little \
star, How I wonder what you are'
s1 =  'Twinkle, twinkle, little star, \n\tHow I wonder what you are! \nUp above the world so high, \n\tLike a diamond in the sky. \nTwinkle, twinkle, little \
star, \n\tHow I wonder what you are'

print(s1)

#EX5
print('EX5')
l = [23, 4.3, 4.2, 31, "python", 1, 5.3, 9, 1.7]
l.remove('python')
print("list after remove python: ", l)
l.sort()
print("list after sort: ", l)
l.reverse()
print("list after reverse: ", l)
print("4.2 in l: ", 4.2 in l)


#EX6
print('EX6')
# Create three sets: A = {1, 2, 3, 4, 5, 7}, B = {2, 4, 5, 9, 12, 24}, C = {2, 4, 8}
A = {1, 2, 3, 4, 5, 7}
print("A =", A)
B = {2, 4, 5, 9, 12, 24}
print("B = ", B)
set_C = {2, 4, 8}
print("C = ", set_C)
#Iterate over all elements of set C and add each element to A and B
for val in set_C:
    print(val)
    A.add(val)
    B.add(val)
print("A after adding elements:" ,A)
print("B after adding elements:",B)
# intersection
print("Intersection :", A & B)
# union
print("Union :", A | B)
# difference
print("A Difference B:", A - B)
#Print out the maximum value of A union B

print("Maximun A union B:" ,max(A|B))
#Print out the minimum value of A union B
print("Minimun A union B:" ,min(A|B))

#EX7
print("EX7")
#Create a tuple t of 4 elements, that are: 1, 'python', [2, 3], (4, 5)
T =(1, 'python', [2, 3], (4, 5))
print(T)
print("unpack the tuple")
(t1, t2, [t3,t4], (t5,t6)) = T

print("T",T)
print("last item",t6)

T = (T,[2,3])
print("T",T)

a = [2,3,4]
b = [5,6,7]
c = map(lambda x,y:(x,y),a,b)
print(c)

#EX8
print("EX8")
#a Write a Python script to concatenate following dictionaries to create a new one.

dic1={1:10, 2:20}
dic2={3:30, 4:40}
dic3={5:50, 6:60}
dict4 = dict(dic1)
dict4.update(dic2)
dict4.update(dic3)
print("A new dict after concatenate: ", dict4)

#Write a Python script to print a dictionary where the keys are numbers between 1 and 15 (both included)
#and the values are square of keys.


squareDic = {}
for key in range(1, 16):
    squareDic.update({key:key**2})
squareDic
print(squareDic)

squareDic_2 = {x: x**2 for x in range(1,16)}
squareDic_2
print(squareDic_2)

#b Write a Python script to sort ascending a dictionary by value.
#input= {'a': 1, 'b': 4, 'c': 2}
d = {'a':1, 'b':4, 'c':2}
print(sorted(d, key=d.get))


import operator
dic = {'a':1, 'b':4, 'c':2}
dic_sorted = sorted(dic.items(), key = operator.itemgetter(1))
print(dic_sorted)
#c
s = "Python is an easy language to learn".replace(' ','')
d = {}
for i in s:
    if i not in d:
        d[i]=1
    else:
        d[i]=d[i]+1
print(d)